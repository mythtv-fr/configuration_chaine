#!/usr/bin/env perl
use strict; 
use warnings;

use HTML::TreeBuilder::XPath;

# configurer le fichier de sauvegarde
	my $home = $ENV{HOME};
	$home = '.' if not defined $home;
	my $conf_dir = "$home/mythtv/ftp";
	(-d $conf_dir) or mkdir($conf_dir, 0777)
	or die "cannot mkdir $conf_dir: $!";
	my $config_file ="bouquets_sat_eu";
	
# ouvrir le fichier de sauvegarde
	open(CONF, "> $config_file") or die "Cannot write to $config_file: $!";
 
# recuperation des données

	my $url = ("http://fr.kingofsat.fr/packages.php");
	my $tree = HTML::TreeBuilder::XPath->new_from_url($url);
	 
	my @elements = map { $_->parent } 
	  @{ $tree->findnodes('//tr[@bgcolor]/td') };
 
	my @results;
	for my $element (@elements) {
	  push @results, [ map { $_->as_text} $element->find('td') ]
	}

	my ($x0,$x1,$n) =(0,0,0);
	foreach my $x (@results) {
		$x0 = $$x[0];
		print CONF "
# liste des bouquets tv sur les satellites européens

# structure du fichier
# $$x[0],$$x[1],nombre $$x[2] de chaines,nombre de $$x[3],$$x[4]
#
# crée par Gilles Choteau à partir de la page 
#  $url


	
"        if (($x0 ne $x1) and ($x0 eq 'Bouquet') and ($n == '0')) ;
		$n++ if $x0 eq 'Bouquet';
		print CONF "$$x[0],$$x[1],$$x[2],$$x[3],$$x[4]\n" if ($x0 ne $x1);
		$x1 = $$x[0];
	}
		
	print "Fichier sauvegardé sous $config_file\n";
	 
0;
 
