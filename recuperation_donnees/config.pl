#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;
use IO::File;
use LWP;
use File::Temp;
use LWP::Simple;
use POSIX;
use utf8; 
use Unicode::Normalize; 
#use Tk;
#use Tk::ProgressBar;
use Storable;
use Storable qw(nstore retrieve); 

my $FichierDonnee='/home/gilles/Dropbox-Gilles/mythtv/liste chaines/pack_canalsat';
	open( FILE, "$FichierDonnee" )
		or die("Impossible d'ouvrir le fichier $FichierDonnee/n$!");
	while (<FILE>) {
		my @tmp = split('\,', $_);   
		my $name = $tmp[3];
		$name = CleanNom($name);
		print "$name\n";
	}




#----------------------------------------------------------------------------------------------------------------------------
# nettoyage des noms de chaines pour comparaison entre les diffrentes sources

sub CleanNom
{
	my ($E)=shift;
	$E = NFKD $E; 											#supprime les accents
	$E =~ s/\pM//g; 											#supprime les accents											
	$E =uc $E ; 												# met en majuscule
	$E =~ s/13EME/13E/;										# renommer certains termes
	$E =~ s/EXTREM /EXTREME/;
	$E =~ s/ENCYCLOPEDIA/ENCYCLO/;
	$E =~ s/NAT GEO WILD/NATIONALGEOGRAPHICWILD/;
	$E =~ s/DE LA LOIRE/DE LOIRE/;
	$E =~ s/LA CHAINE PARLEMENTAIRE/LCP/;
	$E =~ s/SCIFI/SYFY UNIVERSAL/;
	my @i =  ( ' FRANCE', ' EUROPE', ' SAT', 'FRANCAIS', 'UK', 'AMERICA', ' INTERNATIONAL', '\(' , '\)', '\.', '\>', '\-', 'ENTERTAINMENT',
		 'LA CHAINE INFO', 'CHANNEL', 'OLYMPIQUE', 'LYONNAIS', 'FBS', 'ROUSSILLON');
	for my $i (@i)
		{
			$E =~ s/$i//;                                                                            # supprimer les termes definis dans @i
		}
	$E =~ s/\s+//g;										# supprime les espaces
	$E =~ s/.*RADIO.*//;										# suppression des radios
	return ( $E)
}

#----------------------------------------------------------------------------------------------------------------------------
# nettoyage des noms de chaines pour comparaison entre les diffrentes sources
sub CleanNom1
{
	my $E=shift;
	my @i =  ( ' France', " Fran.ais", ' Sat');
	for my $i (@i)
	{
		$E =~ s/$i//;                                                                            # supprimer les termes definis dans @i
	}
return ($E)	
}
