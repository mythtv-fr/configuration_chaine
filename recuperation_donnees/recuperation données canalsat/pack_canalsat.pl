#!/usr/bin/env perl
use strict; 
use warnings;
use HTML::TreeBuilder::XPath;
use Data::Dumper;


# configurer le fichier de sauvegarde
	my $home = $ENV{HOME};
	$home = '.' if not defined $home;
	my $conf_dir = "$home";
	(-d $conf_dir) or mkdir($conf_dir, 0777)
	or die "cannot mkdir $conf_dir: $!";
	my $config_file ="pack_canalsat.txt";
	

#calcul de date
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	my $date =  $mday."/".($mon+1)."/".(1900+$year);# recuperation des données	
	my $url = ("http://www.lesoffrescanal.fr/les-packs-canalsat#here");
#	my $tree = HTML::TreeBuilder::XPath->new_from_url($url);
my $html = do {local $/; <DATA>};
my $tree = HTML::TreeBuilder::XPath->new();
$tree->parse( $html );
	
# ouvrir le fichier de sauvegarde
	open(CONF, "> $config_file") or die "Cannot write to $config_file: $!";
	print CONF"
# liste des chaines par options d'abonnement canalsat

# structure du fichier
# fournisseur, bouquet, genre, nom de chaine
#
# crée le $date par Gilles Choteau à partir de la page 
#  $url
#
";

my @elements = map { $_->parent } 
	  @{ $tree->findnodes('.//dl[@class="row-category-channel active first"]' or './/dl[@class="row-category-channel"]')};
	  
	  
	foreach  my $element (@elements) {
		my $bouquet = $element ->findnodes('./@id');
		$bouquet =~s/pack\_//;
		$bouquet =~s/\-channels//;
		$bouquet =~s/\_/\-/;
		$bouquet =~ s/\s+//g;
#		print "bouquet = $bouquet\n";

		foreach my $channels ($element ->findnodes('.//dl')) {
			foreach my $them ($channels ->findnodes('.//dt/a/span[2]')){
				my $theme= $them->as_text();
#				print "\ttheme : $theme\n";
#			
				foreach my $channel ($channels ->findnodes ( './/div[@class="name-channel"]/span/a')){
					$channel= $channel->as_text();
#				print "\t\tchaine = $channel\n";

				print CONF "canalsat,$bouquet,$theme,$channel\n";
				}
			}
		}
	}

	print "Fichier sauvegardé sous $config_file\n";	


__DATA__


<!DOCTYPE html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Les offres canal Les packs  - Les Offres Canal</title>
<meta name="description" content="Les Offres Canal. Abonnement Canal+ et  CanalSat sur la TNT, le satellite, l'ADSL, le cable, la xBox ou les smart Phones." />
<meta name="keywords" content="Canal+, CanalSat, abonnement, Canal sur xBox" />
<meta name="robots" content="INDEX,FOLLOW" />
<meta name="msapplication-tap-highlight" content="no" /> 
<link rel="icon" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/favicon.ico" type="image/x-icon" />
<!--[if lt IE 7]>
<script type="text/javascript">
//<![CDATA[
    var BLANK_URL = 'http://www.lesoffrescanal.fr/js/blank.html';
    var BLANK_IMG = 'http://www.lesoffrescanal.fr/js/spacer.gif';
//]]>
</script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/js/prototype/windows/themes/tooltip_skin.css" />
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/widgets.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/lnkWcb.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/styles-opp.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/bootstrap.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/style-2.css" media="all" />
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/print.css" media="print" />
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/jquery/jquery.min.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/prototype/prototype.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/lib/ccard.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/prototype/validation.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/scriptaculous/builder.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/scriptaculous/effects.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/scriptaculous/dragdrop.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/scriptaculous/controls.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/scriptaculous/slider.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/scriptaculous/accordion.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/prototype/livepipe.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/prototype/scrollbar.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/prototype/protoslider/protoslider.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/varien/js.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/varien/form.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/varien/menu.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/mage/translate.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/js/mage/cookies.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/html5shiv.min.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/window.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/tooltip_manager.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/default/js/scripts.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/widget-scripts.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/common-scripts.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/protocheck.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/css_browser_selector.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/swfobject.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/player_api.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/enterprise/catalogevent.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/default/js/enterprise/wishlist.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/jquery.lazyload.min.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/cube.js"></script>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/css3-mediaqueries.js"></script>
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/css/styles-ie.css" media="all" />
<![endif]-->
<!--[if lt IE 7]>
<script type="text/javascript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/default/js/iehover-fix.js"></script>
<![endif]-->

<script type="text/javascript">
//<![CDATA[
Mage.Cookies.path     = '/';
Mage.Cookies.domain   = '.lesoffrescanal.fr';
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
optionalZipCountries = [];
//]]>
</script>
<script type="text/javascript">//<![CDATA[
        var Translator = new Translate({"HTML tags are not allowed":"Les balises HTML ne sont pas autoris\u00e9es","Please select an option.":"Veuillez s\u00e9lectionner une option.","This is a required field.":"Ceci est un champ obligatoire.","Please enter a valid number in this field.":"Veuillez saisir un nombre valide.","The value is not within the specified range.":"Cette valeur ne fait pas partie de la marge accept\u00e9e.","Please use numbers only in this field. Please avoid spaces or other characters such as dots or commas.":"Veuillez n'utiliser que des chiffres dans ce champ. Evitez les espaces et autres caract\u00e8res tels que des points ou des virgules.","Please use letters only (a-z or A-Z) in this field.":"Veuillez n'utiliser que des lettres (a-z ou A-Z) dans ce champ.","Please use only letters (a-z), numbers (0-9) or underscore(_) in this field, first character should be a letter.":"Veuillez utiliser seulement des lettres (a-z), des chiffres (0-9) ou le tiret bas (_) dans ce champ, le premier caract\u00e8re doit \u00eatre une lettre.","Please use only letters (a-z or A-Z) or numbers (0-9) only in this field. No spaces or other characters are allowed.":"Veuillez uniquement utiliser des lettres (a-z ou A-Z) ou des chiffres (0-9) dans ce champ. Aucune espace ou aucun autre caract\u00e8re n'est autoris\u00e9.","Please use only letters (a-z or A-Z) or numbers (0-9) or spaces and # only in this field.":"Veuillez uniquement utiliser des lettres (a-z ou A-Z) ou des chiffres (0-9) ou des espaces et # uniquement dans ce champ.","Please enter a valid phone number. For example (123) 456-7890 or 123-456-7890.":"Veuillez saisir un num\u00e9ro de t\u00e9l\u00e9phone valide.","Please enter a valid fax number. For example (123) 456-7890 or 123-456-7890.":"Veuillez entrer un num\u00e9ro de fax valide. Par exemple (123) 456-7890 ou 123-456-7890.","Please enter a valid date.":"Veuillez entrer une date valide.","Please enter a valid email address. For example johndoe@domain.com.":"Veuillez saisir une adresse email valide.","Please use only visible characters and spaces.":"Veuillez uniquement utiliser des espaces et des caract\u00e8res visibles.","Please enter 6 or more characters. Leading or trailing spaces will be ignored.":"Veuillez entrer 6 caract\u00e8res ou plus. Des espaces en d\u00e9but ou en fin seront ignor\u00e9s.","Please enter 7 or more characters. Password should contain both numeric and alphabetic characters.":"Veuillez entrer 7 caract\u00e8res ou plus. Le mot de passe doit contenir des lettres et des chiffres.","Please make sure your passwords match.":"Veuillez v\u00e9rifier que votre mot de passe fonctionne.","Please enter a valid URL. Protocol is required (http:\/\/, https:\/\/ or ftp:\/\/)":"Veuillez entrer une URL valide. Le protocole est n\u00e9cessaire (http:\/\/, https:\/\/ ou ftp:\/\/)","Please enter a valid URL. For example http:\/\/www.example.com or www.example.com":"Veuillez saisir une adresse Internet valide. Par exemple http:\/\/www.exemple.com ou www.exemple.com","Please enter a valid URL Key. For example \"example-page\", \"example-page.html\" or \"anotherlevel\/example-page\".":"Veuillez entrer une cl\u00e9 d'URL valide. Par exemple,  \"page-exemple\", \"page-exemple.html\" ou \"autreniveau\/page-exemple\".","Please enter a valid XML-identifier. For example something_1, block5, id-4.":"Veuillez entrer un identifiant XML valide. Par exemple, objet_1, bloc5, id-4.","Please enter a valid social security number. For example 123-45-6789.":"Veuillez entrer un num\u00e9ro de s\u00e9curit\u00e9 sociale valide. Par exemple 123-54-6789.","Please enter a valid zip code. For example 90602 or 90602-1234.":"Veuillez entrer un code postal valide. Par exemple 90602 ou 90602-1234.","Please enter a valid zip code.":"Veuillez entrer un code postal valide (France m\u00e9tropolitaine)","Please use this date format: dd\/mm\/yyyy. For example 17\/03\/2006 for the 17th of March, 2006.":"Veuillez utiliser ce format de date: dd\/mm\/yyyy. Par exemple 29\/04\/1960 pour le 29th of April, 1960.","Please enter a valid $ amount. For example $100.00.":"Veuillez entrer un montant en $ valide. Par exemple 100 $.","Please select one of the above options.":"Veuillez s\u00e9lectionner l'un des choix ci-dessus.","Please select one of the options.":"Veuillez s\u00e9lectionner l'une des options.","Please select State\/Province.":"Veuillez choisir l'\u00e9tat\/la province.","Please enter a number greater than 0 in this field.":"Veuillez entrer un nombre plus grand que 0 dans ce champ.","Please enter a number 0 or greater in this field.":"Veuillez entrer un nombre \u00e9gal ou sup\u00e9rieur \u00e0 0 dans ce champ.","Please enter a valid credit card number.":"Veuillez entrer un num\u00e9ro de carte de cr\u00e9dit valide.","Credit card number does not match credit card type.":"Le num\u00e9ro de la carte de cr\u00e9dit ne correspond pas au type de la carte de cr\u00e9dit.","Card type does not match credit card number.":"Le type de la carte ne correspond pas au num\u00e9ro de la carte de cr\u00e9dit.","Incorrect credit card expiration date.":"Date d'expiration de la carte incorrecte.","Please enter a valid credit card verification number.":"Veuillez entrer un num\u00e9ro de v\u00e9rification de carte de cr\u00e9dit valide.","Please use only letters (a-z or A-Z), numbers (0-9) or underscore(_) in this field, first character should be a letter.":"Veuillez n'utiliser que des lettres (a-z ou A-Z),  des nombres (0-9) ou le trait pour souligner (_) dans ce champ, le premier caract\u00e8re devant \u00eatre obligatoirement une lettre.","Please input a valid CSS-length. For example 100px or 77pt or 20em or .5ex or 50%.":"Veuillez entrer une longueur CSS valide. Par exemple, 00\u00a0px ou 77\u00a0pt ou 20\u00a0em ou 0,5\u00a0ex ou 50\u00a0%.","Text length does not satisfy specified text range.":"La longueur du texte ne correspond pas aux attentes sp\u00e9cifi\u00e9es.","Please enter a number lower than 100.":"Veuillez entrer un nombre inf\u00e9rieur \u00e0 100.","Please select a file":"Veuillez s\u00e9lectionner un fichier","Please enter issue number or start date for switch\/solo card type.":"Veuillez entrer le num\u00e9ro de probl\u00e8me ou la date de d\u00e9but pour le type de carte Switch\/Solo.","Please wait, loading...":"En cours de chargement, veuillez patienter...","This date is a required value.":"Cette date est une valeur requise.","Please enter a valid day (1-%d).":"Veuillez entrer un jour valide (1-%d).","Please enter a valid month (1-12).":"Veuillez entrer un mois valide (1-12).","Please enter a valid year (1900-%d).":"Veuillez entrer une ann\u00e9e valide (1900-%d).","Please enter a valid full date":"Veuillez entrer une date compl\u00e8te valide","Please enter a valid date between %s and %s":"Veuillez entrer une date valide entre %s et %s","Please enter a valid date equal to or greater than %s":"Veuillez entrer une date valide \u00e9galement ou sup\u00e9rieure \u00e0 %s","Please enter a valid date less than or equal to %s":"Veuillez entrer une date valide inf\u00e9rieure ou \u00e9gale \u00e0 %s","Complete":"Terminer","Please choose to register or to checkout as a guest":"Veuillez choisir de vous enregistrer ou de passer \u00e0 la caisse en tant qu'invit\u00e9","Your order cannot be completed at this time as there is no shipping methods available for it. Please make necessary changes in your shipping address.":"Votre commande ne peut pas \u00eatre r\u00e9alis\u00e9e pour le moment \u00e9tant donn\u00e9 qu'aucune m\u00e9thode de livraison n'est disponible. Veuillez apporter les modifications n\u00e9cessaires \u00e0 votre adresse de livraison.","Please specify payment method.":"Veuillez pr\u00e9ciser une m\u00e9thode de paiement.","Your order cannot be completed at this time as there is no payment methods available for it.":"Votre commande ne peut \u00eatre termin\u00e9e pour l\u2019heure, car aucun moyen de paiement n\u2019est disponible pour elle.","Save":"Enregistrer","Cancel":"Annuler","Please use numbers only in this field.":"Veuillez utiliser des chiffres uniquement.","Please enter a valid line number.":"Veuillez saisir un N\u00b0 de t\u00e9l\u00e9phone valide.","Please enter a valid ifc number: 11 or 12 digits starting with 62 or 0817.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 11 chiffres commen\u00e7ant par 62 ou 12 chiffres commen\u00e7ant par 0817.","Please enter a valid ifc number: 11 or 12 digits starting with 044 or 44.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 11 ou 12 chiffres commen\u00e7ant par 044 ou 44.","Please enter a valid ifc number: 11 or 12 digits starting with 045 or 45.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 11 ou 12 chiffres commen\u00e7ant par 045 ou 45.","Please enter a valid ifc number: 14 digits starting with 033, 038 or 077.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 14 chiffres commen\u00e7ant par 033, 038 ou 077.","Please enter a valid ifc number: 12 or 14 digits starting with 026, 033, 038 or 077.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 12 ou 14 chiffres commen\u00e7ant par 026, 033, 038 ou 077.","Please enter a valid ifc number: 11 or 12 digits starting with 033, 33, 058 or 58.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 11 ou 12 chiffres commen\u00e7ant par 033, 33, 058 ou 58.","Please enter a valid ifc number: 11 or 12 digits starting with 085 or 85.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 11 ou 12 chiffres commen\u00e7ant par 085 ou 85.","Please enter a valid ifc number: 11 digits starting with 36 or 14 digits starting with 038.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 11 chiffres commen\u00e7ant par 36 ou 14 chiffres commen\u00e7ant par 038.","Please enter a valid ifc number: 14 digits starting with 036.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 14 chiffres commen\u00e7ant par 036.","Please enter a valid ifc number: 12 digits starting with 0817.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 12 chiffres commen\u00e7ant par 0817.","Please enter a valid ifc number: 11 digits starting with 817.":"Veuillez saisir un num\u00e9ro de mat\u00e9riel valide : 11 chiffres commen\u00e7ant par 817.","Please make sure your emails match.":"Veuillez entrer la m&ecirc;me adresse email.","Please enter a valid full date.":"La date de naissance est incompl&egrave;te.","Minimun length unreached.":"La taille minimum est incorrecte.","Incorrect RIB key.":"Cl\u00e9 RIB incorrect.","Incorrect IBAN key.":"Cl\u00e9 IBAN incorrect.","Please enter a valid day (1-31).":"Veuillez saisir un jour valide (1-31).","Please enter a valid day (1-30).":"Veuillez saisir un jour valide (1-30).","Please enter a valid day (1-29).":"Veuillez saisir un jour valide (1-29).","Please enter a valid day (1-28).":"Veuillez saisir un jour valide (1-28).","Please enter a valid year (1900-2011).":"Veuillez saisir une ann\u00e9e valide (1900-2011).","Please enter a valid card number: 17 digits.":"Le format du num\u00e9ro saisi est incorrect.","Please specify a relay point.":"Choisissez un Point Relais Colis.","Please enter a valid card number: 11 digits.":"Veuillez saisir un num\u00e9ro de carte valide : 11 chiffres."});
        //]]></script><script type="text/javascript">
//<![CDATA[
Event.observe(window, 'load', testOs);
//]]>
</script>
<script type="text/javascript" src="//d2yp9b3a29g3i2.cloudfront.net/ergotest/1c383cd30b.js"></script>

    
</head>
    <body class=" cms-page-view cms-les-packs-canalsat">

        <!-- BEGIN GOOGLE ANALYTICS CODEs -->
<script type="text/javascript">
//<![CDATA[
    var _gaq = _gaq || [];
    
_gaq.push(['_setAccount', 'UA-41129959-1']);
_gaq.push(['_trackPageview']);
    
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

//]]>
</script>
<!-- END GOOGLE ANALYTICS CODE -->
        
<header id="headerContainer" class="container">
    <div>
                <p class="logo">
            <a href="http://www.lesoffrescanal.fr/" title="Les offres Canal" class="logo">
                <img src="http://www.lesoffrescanal.fr/media/cms/logos/logo.png" alt="Les offres Canal" />
            </a>
        </p>
        
        <div id="headerLinks">
            
<ul>
    <li>
        <a href="#here" title="" id="promoCodeLink">J'ai un code promo</a>
        <div id="promoCodePopin">
            <span class="triangle"></span>
            <p class="popinTitle">Vous avez reçu un code promo</p>
            <p>par email ou par courrier</p>
            <p id="promocode-message" class="error-msg"></p>
            <form method="post" id="promocode-form" action="">
                                <input type="hidden" name="current_url" value="http://www.lesoffrescanal.fr:8080/les-packs-canalsat"/>
                <input type="hidden" name="modal_form" value="1"/>
                <input type="text" name="coupon" id="promocode-text" placeholder="Renseigner votre code promo" />
                <input type="button" class="button grey" value="valider" id="promocode-button" onclick="promoCode.call()"/>
            </form>
        </div>
    </li>
    <li>
        <a href="http://www.lesoffrescanal.fr/sales/order/login/#sc_intcmp=LOC:HOME:MENUDROITE:DEJAABONNESUIVICDE" title="">Commande en cours ou Abonné</a>
    </li>
                    <li class="subscriptionLink"><a onclick='callToAction("event22", "s.eVar60", "LOC:CSATPACKS:SABONNE")' href="http://www.lesoffrescanal.fr/checkout/tunnel/#sc_intcmp=LOC::ETAPE0:SABONNER" class="arrow"> Je m'abonne</a></li>                
        </ul>
<script type="text/javascript">
//<![CDATA[
    var promoCode = new Promocode('promocode-form', "http://www.lesoffrescanal.fr/checkout/tunnel/landing/" );
    
    $('promocode-text').observe('keypress', bindPromocodePost);
    function bindPromocodePost(evt){
        if (evt.keyCode == Event.KEY_RETURN) {
            promoCode.call();
            Event.stop(evt);
        }
    }
    
//]]>
</script>

<script type="text/javascript">
    +function($) {
    	// Popin management
        $('#promoCodeLink').click(function(e) {
            $("#promocode-message").empty();
            $("#promocode-message").hide();
        });
    }(jQuery);
</script>



                </div>
        <script src="//assets.adobedtm.com/75b97122605cbde107b38740210932f5c290a210/satelliteLib-b2476d6739aae9545829a88f6a192e8ed601842c.js"></script>
                    <script src="http://media.canal-plus.com/cnil/cnil.js"></script>
            </div>
</header>
<nav id="menuContainer" class="container"> 
    <div>     
        

<a href="http://www.lesoffrescanal.fr/" title="" class="homeLink">Accueil</a>
<ul id="menu">
    <li><a title="" href="http://www.lesoffrescanal.fr/offre-tv">TOUTES NOS OFFRES</a></li><li><a title="" href="http://www.lesoffrescanal.fr/service-canal-plus">Services inclus</a></li><li><a title="" href="http://www.lesoffrescanal.fr/modes-de-reception">Modes de réception</a></li></ul>

                       
    <p class="subscriptionLink"><a onclick='callToAction("event22", "s.eVar60", "LOC:CSATPACKS:SABONNE")' href="http://www.lesoffrescanal.fr/checkout/tunnel/#sc_intcmp=LOC::ETAPE0:SABONNER"> Je m'abonne</a></p>                
        <div id="inqC2CImgContainer" style="display: inline;">&nbsp;</div>          
    </div>
</nav>

<script type="text/javascript">
    +function($) {
        var stickyHeaderOffset = 200,
            stickyMenu = $('#menuContainer'),
            $window = $(window);
        
        var setStickyMenu = function() {
            var scrollTop = $window.scrollTop();
            if(scrollTop > stickyHeaderOffset && !stickyMenu.hasClass('presticky')) {
                stickyMenu.addClass('presticky');
                stickyTimeout = setTimeout(function() {
                     stickyMenu.addClass('sticky');
                }, 500);
            }
            else if(scrollTop <= stickyHeaderOffset && stickyMenu.hasClass('presticky')) {
                var newSticky = stickyMenu.clone(true).removeClass('presticky sticky');
                stickyMenu.replaceWith(newSticky);
                stickyMenu = newSticky;
                clearTimeout(stickyTimeout);
            }
        };

        /*
         * Sets the page scroll to display menu "fixed" when first menu is scrolled
         */
        $window.on('scroll', setStickyMenu);
    }(jQuery);
</script>   
                <div class="container" id="breadcrumbs" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
    <ol>
                     <!-- Home Link -->
                    <li class="home">
                        <a href="http://www.lesoffrescanal.fr/" itemprop="url" title="Aller à la page d'accueil">
                            <span itemprop="title">Home</span>
                        </a>
                    </li>
                                                 <!-- Last : No link -->
                    <li>
                        <h1 itemprop="title">Les packs</h1>
                    </li>
                                        </ol>
</div>

        <div role="main">
                        <div class="std">&nbsp;</div>

<div class="container" id="packChannelOptions">
    <div>
		<h2>LES PACKS DE CANALSAT</h2>
		<br/>
		<p style="margin-left:50px;">
			<span style="font-size: xx-small;">
				Les chaines <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/> sont disponibles uniquement avec CANALSAT par satellite et ADSL
			</span>
		</p>
        <ul id="packChannelOptionTabList">
                            <li class="selected" data-channel="option1">
                    <p id="pack_panorama" >
						<span onclick="callEulerianInAjaxPage('packs','LOC - CSAT - Formules - PACK PANORAMA');callHavasInAjaxPage('packs','LOC - CSAT - Formules - PACK PANORAMA');callToActionWithMathsOperator('', 'les-packs', 'LOC - CSAT - Formules - PACK PANORAMA')" class="title">
							PANORAMA						</span>
                    </p>                          
                </li>
                            <li  data-channel="option2">
                    <p id="pack_series_cinema" >
						<span onclick="callEulerianInAjaxPage('packs','LOC - CSAT - Formules - PACK SERIES CINEMA');callHavasInAjaxPage('packs','LOC - CSAT - Formules - PACK SERIES CINEMA');callToActionWithMathsOperator('', 'les-packs', 'LOC - CSAT - Formules - PACK SERIES CINEMA')" class="title">
							GRAND PANORAMA						</span>
                    </p>                          
                </li>
                    </ul>
                    <div id="pack_panorama-channels" class="packChannelOptionContent option1  active">
                                                                            <dl class="row-category-channel active first" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Divertissement                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/jimmy.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_10.PNG" alt="JIMMY" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/jimmy.html"> JIMMY</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/serie-club.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_319.PNG" alt="SERIE CLUB" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/serie-club.html"> SERIE CLUB</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/comedie.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_70.PNG" alt="COMEDIE+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/comedie.html"> COMEDIE+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/13eme-rue.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_69.PNG" alt="13EME RUE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/13eme-rue.html"> 13EME RUE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/13-rue-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_342.PNG" alt="13EME RUE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/13-rue-hd.html"> 13EME RUE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/syfy-universal.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_243.PNG" alt="SYFY" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/syfy-universal.html"> SYFY</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/syfy-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_341.PNG" alt="SYFY HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/syfy-hd.html"> SYFY HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tf6.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1017.PNG" alt="TF6" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tf6.html"> TF6</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tf6-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_354.PNG" alt="TF6 HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tf6-hd.html"> TF6 HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_39.PNG" alt="MTV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv.html"> MTV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_470.PNG" alt="MTV HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-hd.html"> MTV HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/non-stop-people.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_434.PNG" alt="NON STOP PEOPLE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/non-stop-people.html"> NON STOP PEOPLE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/june.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_127.PNG" alt="JUNE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/june.html"> JUNE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/paris-premiere.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_17.PNG" alt="PARIS PREMIERE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/paris-premiere.html"> PARIS PREMIERE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/paris-premiere-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_294.PNG" alt="PARIS PREMIERE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/paris-premiere-hd.html"> PARIS PREMIERE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/jone.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_486.PNG" alt="J ONE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/jone.html"> J ONE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/game-one.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_12.PNG" alt="GAME ONE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/game-one.html"> GAME ONE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mcm.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_11.PNG" alt="MCM" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mcm.html"> MCM</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ab-1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_76.PNG" alt="AB1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ab-1.html"> AB1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/rtl9.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_212.PNG" alt="RTL9" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/rtl9.html"> RTL9</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv-breizh.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_85.PNG" alt="TV BREIZH" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv-breizh.html"> TV BREIZH</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/teva-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_292.PNG" alt="TEVA HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/teva-hd.html"> TEVA HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/e-entertainment.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_126.PNG" alt="E! ENTERTAINMENT" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/e-entertainment.html"> E! ENTERTAINMENT</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mangas.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_200.PNG" alt="MANGAS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mangas.html"> MANGAS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/teva.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_221.PNG" alt="TEVA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/teva.html"> TEVA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Sport                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/l-equipe-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_84.PNG" alt="L'EQUIPE TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/l-equipe-tv.html"> L'EQUIPE TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-life.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_281.PNG" alt="EQUIDIA LIFE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-life.html"> EQUIDIA LIFE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-live.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_44.PNG" alt="EQUIDIA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-live.html"> EQUIDIA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/sport.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_72.PNG" alt="SPORT+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/sport.html"> SPORT+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eurosport.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_4.PNG" alt="EUROSPORT" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eurosport.html"> EUROSPORT</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eusrosport-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_101.PNG" alt="EUROSPORT HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eusrosport-hd.html"> EUROSPORT HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/infosport.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1025.PNG" alt="INFOSPORT+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/infosport.html"> INFOSPORT+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/sport-365.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_426.PNG" alt="SPORT 365" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/sport-365.html"> SPORT 365</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eurosport-2.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_314.PNG" alt="EUROSPORT 2" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eurosport-2.html"> EUROSPORT 2</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eurosport-2-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_436.PNG" alt="EUROSPORT 2 HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eurosport-2-hd.html"> EUROSPORT 2 HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ma-chaine-sport.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_115.PNG" alt="MA CHAINE SPORT" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ma-chaine-sport.html"> MA CHAINE SPORT</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ma-chaine-sport-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_298.PNG" alt="MA CHAINE SPORT HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ma-chaine-sport-hd.html"> MA CHAINE SPORT HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/kombat-sport-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_410.PNG" alt="KOMBAT SPORT HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/kombat-sport-hd.html"> KOMBAT SPORT HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/motors-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_213.PNG" alt="MOTORS TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/motors-tv.html"> MOTORS TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/om-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_104.PNG" alt="OM TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/om-tv.html"> OM TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ol-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_234.PNG" alt="OL TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ol-tv.html"> OL TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/onzeo.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_304.PNG" alt="ONZEO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/onzeo.html"> ONZEO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/girondins-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_148.PNG" alt="GIRONDINS TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/girondins-tv.html"> GIRONDINS TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nautical-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_316.PNG" alt="NAUTICAL CHANNEL" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nautical-channel.html"> NAUTICAL CHANNEL</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Jeunesse                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/gulli.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_248.PNG" alt="GULLI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/gulli.html"> GULLI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/disney-junior.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_224.PNG" alt="DISNEY JUNIOR" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/disney-junior.html"> DISNEY JUNIOR</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tiji.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_112.PNG" alt="TIJI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tiji.html"> TIJI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nickelodeon-junior.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_173.PNG" alt="NICKELODEON JR" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nickelodeon-junior.html"> NICKELODEON JR</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/piwi.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1026.PNG" alt="PIWI+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/piwi.html"> PIWI+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/boomerang.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_330.PNG" alt="BOOMERANG" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/boomerang.html"> BOOMERANG</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nickelodeon.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_240.PNG" alt="NICKELODEON" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nickelodeon.html"> NICKELODEON</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/canal-j.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_13.PNG" alt="CANAL J" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/canal-j.html"> CANAL J</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/disney-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_51.PNG" alt="DISNEY CHANNEL" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/disney-channel.html"> DISNEY CHANNEL</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/disney-channel-1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_226.PNG" alt="DISNEY CHANNEL+1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/disney-channel-1.html"> DISNEY CHANNEL+1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cartoon-network.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_36.PNG" alt="CARTOON NETWORK" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cartoon-network.html"> CARTOON NETWORK</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/disney-xd-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_284.PNG" alt="DISNEY XD HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/disney-xd-hd.html"> DISNEY XD HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tele-toon.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1020.PNG" alt="TELETOON+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tele-toon.html"> TELETOON+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tele-toon-1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_317.PNG" alt="TELETOON+1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tele-toon-1.html"> TELETOON+1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/boing.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_287.PNG" alt="BOING" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/boing.html"> BOING</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Découverte                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/discovery-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_125.PNG" alt="DISCOVERY" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/discovery-channel.html"> DISCOVERY</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_262.PNG" alt="TV8 MONT-BLANC" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html"> TV8 MONT-BLANC</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/rmc-decouverte.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_438.PNG" alt="RMC DECOUVERTE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/rmc-decouverte.html"> RMC DECOUVERTE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_6.PNG" alt="PLANETE+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete.html"> PLANETE+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete-thalassa.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_223.PNG" alt="PLANETE+THALASSA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete-thalassa.html"> PLANETE+THALASSA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete-ci.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_336.PNG" alt="PLANETE+ JUSTICE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete-ci.html"> PLANETE+ JUSTICE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete-ae.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_209.PNG" alt="PLANETE+ NOLIMIT" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete-ae.html"> PLANETE+ NOLIMIT</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/discovery-science.html">
                                <div class="image">
                                    <img src="http://static2.canalplus.fr/CHNWEB/PNG/60X45/CHNWEB_390.PNG" alt="DISCOVERY SCIENCE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/discovery-science.html"> DISCOVERY SCIENCE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/national-geographic-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_201.PNG" alt="NATIONAL GEO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/national-geographic-channel.html"> NATIONAL GEO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nat-geo-wild-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_290.PNG" alt="NAT GEO WILD HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nat-geo-wild-hd.html"> NAT GEO WILD HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/voyage.html">
                                <div class="image">
                                    <img src="http://static2.canalplus.fr/CHNWEB/PNG/60X45/CHNWEB_477.PNG" alt="VOYAGE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/voyage.html"> VOYAGE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ushuaia-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_320.PNG" alt="USHUAIA TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ushuaia-tv.html"> USHUAIA TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/histoire.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_210.PNG" alt="HISTOIRE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/histoire.html"> HISTOIRE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/toute-l-histoire.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_78.PNG" alt="TOUTE L'HISTOIRE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/toute-l-histoire.html"> TOUTE L'HISTOIRE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Musique                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj-hits.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_339.PNG" alt="NRJ HITS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj-hits.html"> NRJ HITS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-base.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_346.PNG" alt="MTV BASE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-base.html"> MTV BASE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-pulse.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_235.PNG" alt="MTV PULSE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-pulse.html"> MTV PULSE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-idol.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_236.PNG" alt="MTV IDOL" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-idol.html"> MTV IDOL</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/trace-urban.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_65.PNG" alt="TRACE URBAN" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/trace-urban.html"> TRACE URBAN</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mcm-pop.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_114.PNG" alt="MCM POP" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mcm-pop.html"> MCM POP</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mcm-top.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_34.PNG" alt="MCM TOP" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mcm-top.html"> MCM TOP</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/brava-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_165.PNG" alt="BRAVA HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/brava-hd.html"> BRAVA HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-hits.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_207.PNG" alt="MTV HITS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-hits.html"> MTV HITS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-rocks.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_205.PNG" alt="MTV ROCKS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-rocks.html"> MTV ROCKS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6-music-hits.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_321.PNG" alt="M6 MUSIC" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6-music-hits.html"> M6 MUSIC</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6-music-black.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_343.PNG" alt="M6 MUSIC BLACK" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6-music-black.html"> M6 MUSIC BLACK</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6-music-club.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_344.PNG" alt="M6 MUSIC CLUB" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6-music-club.html"> M6 MUSIC CLUB</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Styles de vie / Info                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                          						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cuisine.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_113.PNG" alt="CUISINE+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cuisine.html"> CUISINE+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/maison.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_305.PNG" alt="MAISON+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/maison.html"> MAISON+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mcs-bien-etre.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_366.PNG" alt="MCS BIEN ETRE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mcs-bien-etre.html"> MCS BIEN ETRE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/lci.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_8.PNG" alt="LCI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/lci.html"> LCI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/la-chaine-meteo.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_9.PNG" alt="LA CHAINE METEO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/la-chaine-meteo.html"> LA CHAINE METEO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                                                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Chaînes TNT                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tf1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_91.PNG" alt="TF1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tf1.html"> TF1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-2.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_92.PNG" alt="FRANCE 2" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-2.html"> FRANCE 2</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-3.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_93.PNG" alt="FRANCE 3" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-3.html"> FRANCE 3</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-5.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_95.PNG" alt="FRANCE 5" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-5.html"> FRANCE 5</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_96.PNG" alt="M6" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6.html"> M6</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/arte.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_94.PNG" alt="ARTE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/arte.html"> ARTE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/direct8.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_231.PNG" alt="D8" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/direct8.html"> D8</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/w9.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_233.PNG" alt="W9" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/w9.html"> W9</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tmc.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_5.PNG" alt="TMC" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tmc.html"> TMC</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nt1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_232.PNG" alt="NT1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nt1.html"> NT1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj12.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_245.PNG" alt="NRJ 12" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj12.html"> NRJ 12</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/lcp.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_89.PNG" alt="LCP" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/lcp.html"> LCP</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-4.html">
                                <div class="image">
                                    <img src="http://newmedia.canal-plus.com/image/01/6/25016.png" alt="FRANCE 4" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-4.html"> FRANCE 4</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bfm-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_247.PNG" alt="BFM TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bfm-tv.html"> BFM TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/i-tele.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_103.PNG" alt="I&gt;TELE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/i-tele.html"> I>TELE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/direct-star.html">
                                <div class="image">
                                    <img src="https://secure-newmedia.canal-plus.com/image/54/8/98548.png" alt="D17" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/direct-star.html"> D17</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/gulli.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_248.PNG" alt="GULLI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/gulli.html"> GULLI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-o.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_82.PNG" alt="FRANCE Ô" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-o.html"> FRANCE Ô</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/l-equipe-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_84.PNG" alt="L'EQUIPE TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/l-equipe-tv.html"> L'EQUIPE TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/numero-23.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_443.PNG" alt="NUMERO 23" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/numero-23.html"> NUMERO 23</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/rmc-decouverte.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_438.PNG" alt="RMC DECOUVERTE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/rmc-decouverte.html"> RMC DECOUVERTE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cherie-25.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_440.PNG" alt="CHERIE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cherie-25.html"> CHERIE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Autres Chaînes                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bbc-world-news.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_99.PNG" alt="BBC WORLD NEWS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bbc-world-news.html"> BBC WORLD NEWS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/best-of-shopping.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_98.PNG" alt="BEST OF SHOPPING" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/best-of-shopping.html"> BEST OF SHOPPING</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bfm-business.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_286.PNG" alt="BFM BUSINESS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bfm-business.html"> BFM BUSINESS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bloomberg-television.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_47.PNG" alt="BLOOMBERG TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bloomberg-television.html"> BLOOMBERG TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cnn.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_30.PNG" alt="CNN INT." />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cnn.html"> CNN INT.</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-life.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_281.PNG" alt="EQUIDIA LIFE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-life.html"> EQUIDIA LIFE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-live.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_44.PNG" alt="EQUIDIA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-live.html"> EQUIDIA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/euronews.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_52.PNG" alt="EURONEWS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/euronews.html"> EURONEWS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-24.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_310.PNG" alt="FRANCE 24 " />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-24.html"> FRANCE 24 </a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/kto.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_102.PNG" alt="KTO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/kto.html"> KTO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span>Disponible uniquement par ADSL</span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6-boutique.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_106.PNG" alt="M6 BOUTIQUE &amp; CO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6-boutique.html"> M6 BOUTIQUE & CO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/montagne-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_263.PNG" alt="MONTAGNE TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/montagne-tv.html"> MONTAGNE TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj-hits.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_339.PNG" alt="NRJ HITS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj-hits.html"> NRJ HITS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj-paris.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_134.PNG" alt="NRJ PARIS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj-paris.html"> NRJ PARIS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv5-monde.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_56.PNG" alt="TV5 MONDE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv5-monde.html"> TV5 MONDE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_262.PNG" alt="TV8 MONT-BLANC" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html"> TV8 MONT-BLANC</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                        </div>
		        <div id="pack_series_cinema-channels" class="packChannelOptionContent option2  ">
                                                                            <dl class="row-category-channel active first" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Divertissement                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/jimmy.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_10.PNG" alt="JIMMY" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/jimmy.html"> JIMMY</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/serie-club.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_319.PNG" alt="SERIE CLUB" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/serie-club.html"> SERIE CLUB</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/comedie.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_70.PNG" alt="COMEDIE+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/comedie.html"> COMEDIE+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/13eme-rue.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_69.PNG" alt="13EME RUE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/13eme-rue.html"> 13EME RUE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/13-rue-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_342.PNG" alt="13EME RUE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/13-rue-hd.html"> 13EME RUE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/syfy-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_341.PNG" alt="SYFY HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/syfy-hd.html"> SYFY HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tf6.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1017.PNG" alt="TF6" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tf6.html"> TF6</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tf6-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_354.PNG" alt="TF6 HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tf6-hd.html"> TF6 HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_39.PNG" alt="MTV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv.html"> MTV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/non-stop-people.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_434.PNG" alt="NON STOP PEOPLE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/non-stop-people.html"> NON STOP PEOPLE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/june.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_127.PNG" alt="JUNE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/june.html"> JUNE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/paris-premiere.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_17.PNG" alt="PARIS PREMIERE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/paris-premiere.html"> PARIS PREMIERE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/jone.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_486.PNG" alt="J ONE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/jone.html"> J ONE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/game-one.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_12.PNG" alt="GAME ONE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/game-one.html"> GAME ONE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mcm.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_11.PNG" alt="MCM" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mcm.html"> MCM</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ab-1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_76.PNG" alt="AB1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ab-1.html"> AB1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/rtl9.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_212.PNG" alt="RTL9" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/rtl9.html"> RTL9</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv-breizh.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_85.PNG" alt="TV BREIZH" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv-breizh.html"> TV BREIZH</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/e-entertainment.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_126.PNG" alt="E! ENTERTAINMENT" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/e-entertainment.html"> E! ENTERTAINMENT</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mangas.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_200.PNG" alt="MANGAS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mangas.html"> MANGAS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/teva.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_221.PNG" alt="TEVA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/teva.html"> TEVA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Sport                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/l-equipe-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_84.PNG" alt="L'EQUIPE TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/l-equipe-tv.html"> L'EQUIPE TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-life.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_281.PNG" alt="EQUIDIA LIFE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-life.html"> EQUIDIA LIFE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-live.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_44.PNG" alt="EQUIDIA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-live.html"> EQUIDIA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/sport.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_72.PNG" alt="SPORT+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/sport.html"> SPORT+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eurosport.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_4.PNG" alt="EUROSPORT" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eurosport.html"> EUROSPORT</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eusrosport-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_101.PNG" alt="EUROSPORT HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eusrosport-hd.html"> EUROSPORT HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/infosport.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1025.PNG" alt="INFOSPORT+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/infosport.html"> INFOSPORT+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/sport-365.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_426.PNG" alt="SPORT 365" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/sport-365.html"> SPORT 365</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eurosport-2.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_314.PNG" alt="EUROSPORT 2" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eurosport-2.html"> EUROSPORT 2</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/eurosport-2-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_436.PNG" alt="EUROSPORT 2 HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/eurosport-2-hd.html"> EUROSPORT 2 HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ma-chaine-sport-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_298.PNG" alt="MA CHAINE SPORT HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ma-chaine-sport-hd.html"> MA CHAINE SPORT HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            /span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/kombat-sport-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_410.PNG" alt="KOMBAT SPORT HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/kombat-sport-hd.html"> KOMBAT SPORT HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/motors-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_213.PNG" alt="MOTORS TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/motors-tv.html"> MOTORS TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/om-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_104.PNG" alt="OM TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/om-tv.html"> OM TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ol-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_234.PNG" alt="OL TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ol-tv.html"> OL TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/onzeo.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_304.PNG" alt="ONZEO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/onzeo.html"> ONZEO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/girondins-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_148.PNG" alt="GIRONDINS TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/girondins-tv.html"> GIRONDINS TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nautical-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_316.PNG" alt="NAUTICAL CHANNEL" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nautical-channel.html"> NAUTICAL CHANNEL</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Jeunesse                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/gulli.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_248.PNG" alt="GULLI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/gulli.html"> GULLI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tiji.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_112.PNG" alt="TIJI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tiji.html"> TIJI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nickelodeon-junior.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_173.PNG" alt="NICKELODEON JR" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nickelodeon-junior.html"> NICKELODEON JR</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/piwi.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1026.PNG" alt="PIWI+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/piwi.html"> PIWI+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/boomerang.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_330.PNG" alt="BOOMERANG" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/boomerang.html"> BOOMERANG</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nickelodeon.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_240.PNG" alt="NICKELODEON" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nickelodeon.html"> NICKELODEON</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/canal-j.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_13.PNG" alt="CANAL J" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/canal-j.html"> CANAL J</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/disney-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_51.PNG" alt="DISNEY CHANNEL" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/disney-channel.html"> DISNEY CHANNEL</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/disney-channel-1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_226.PNG" alt="DISNEY CHANNEL+1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/disney-channel-1.html"> DISNEY CHANNEL+1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cartoon-network.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_36.PNG" alt="CARTOON NETWORK" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cartoon-network.html"> CARTOON NETWORK</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/disney-xd-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_284.PNG" alt="DISNEY XD HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/disney-xd-hd.html"> DISNEY XD HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tele-toon.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_1020.PNG" alt="TELETOON+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tele-toon.html"> TELETOON+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tele-toon-1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_317.PNG" alt="TELETOON+1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tele-toon-1.html"> TELETOON+1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/boing.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_287.PNG" alt="BOING" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/boing.html"> BOING</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Découverte                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/discovery-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_125.PNG" alt="DISCOVERY" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/discovery-channel.html"> DISCOVERY</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_262.PNG" alt="TV8 MONT-BLANC" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html"> TV8 MONT-BLANC</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/rmc-decouverte.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_438.PNG" alt="RMC DECOUVERTE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/rmc-decouverte.html"> RMC DECOUVERTE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_6.PNG" alt="PLANETE+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete.html"> PLANETE+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete-thalassa.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_223.PNG" alt="PLANETE+THALASSA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete-thalassa.html"> PLANETE+THALASSA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete-ci.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_336.PNG" alt="PLANETE+ JUSTICE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete-ci.html"> PLANETE+ JUSTICE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/planete-ae.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_209.PNG" alt="PLANETE+ NOLIMIT" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/planete-ae.html"> PLANETE+ NOLIMIT</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/discovery-science.html">
                                <div class="image">
                                    <img src="http://static2.canalplus.fr/CHNWEB/PNG/60X45/CHNWEB_390.PNG" alt="DISCOVERY SCIENCE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/discovery-science.html"> DISCOVERY SCIENCE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/national-geographic-channel.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_201.PNG" alt="NATIONAL GEO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/national-geographic-channel.html"> NATIONAL GEO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nat-geo-wild-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_290.PNG" alt="NAT GEO WILD HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nat-geo-wild-hd.html"> NAT GEO WILD HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/voyage.html">
                                <div class="image">
                                    <img src="http://static2.canalplus.fr/CHNWEB/PNG/60X45/CHNWEB_477.PNG" alt="VOYAGE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/voyage.html"> VOYAGE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/ushuaia-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_320.PNG" alt="USHUAIA TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/ushuaia-tv.html"> USHUAIA TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/histoire.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_210.PNG" alt="HISTOIRE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/histoire.html"> HISTOIRE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/toute-l-histoire.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_78.PNG" alt="TOUTE L'HISTOIRE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/toute-l-histoire.html"> TOUTE L'HISTOIRE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Musique                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj-hits.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_339.PNG" alt="NRJ HITS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj-hits.html"> NRJ HITS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-base.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_346.PNG" alt="MTV BASE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-base.html"> MTV BASE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-pulse.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_235.PNG" alt="MTV PULSE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-pulse.html"> MTV PULSE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-idol.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_236.PNG" alt="MTV IDOL" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-idol.html"> MTV IDOL</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/trace-urban.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_65.PNG" alt="TRACE URBAN" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/trace-urban.html"> TRACE URBAN</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/brava-hd.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_165.PNG" alt="BRAVA HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/brava-hd.html"> BRAVA HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-hits.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_207.PNG" alt="MTV HITS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-hits.html"> MTV HITS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mtv-rocks.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_205.PNG" alt="MTV ROCKS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mtv-rocks.html"> MTV ROCKS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6-music-black.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_343.PNG" alt="M6 MUSIC BLACK" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6-music-black.html"> M6 MUSIC BLACK</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6-music-club.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_344.PNG" alt="M6 MUSIC CLUB" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6-music-club.html"> M6 MUSIC CLUB</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Styles de vie / Info                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                      						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cuisine.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_113.PNG" alt="CUISINE+" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cuisine.html"> CUISINE+</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/mcs-bien-etre.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_366.PNG" alt="MCS BIEN ETRE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/mcs-bien-etre.html"> MCS BIEN ETRE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/lci.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_8.PNG" alt="LCI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/lci.html"> LCI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/la-chaine-meteo.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_9.PNG" alt="LA CHAINE METEO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/la-chaine-meteo.html"> LA CHAINE METEO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Box-Office                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                              <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-premier.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_14.PNG" alt="CINE+ PREMIER" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-premier.html"> CINE+ PREMIER</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-premier-hd.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_322.PNG" alt="CINE+ PREMIER HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-premier-hd.html"> CINE+ PREMIER HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/orange-cine-max-hd.html">
                                    <div class="image">
                                        <img src="http://static2.canalplus.fr/CHNWEB/PNG/60X45/CHNWEB_373.PNG" alt="OCS MAX HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/orange-cine-max-hd.html"> OCS MAX HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/ocs-city.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_467.PNG" alt="OCS NOVO HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/ocs-city.html"> OCS NOVO HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                    						
                                                                                                                                                                                                                                             
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Action/Passions                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                              <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-frisson.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_25.PNG" alt="CINE+ FRISSON" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-frisson.html"> CINE+ FRISSON</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-frisson-hd.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_398.PNG" alt="CINE+ FRISSON HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-frisson-hd.html"> CINE+ FRISSON HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-emotion.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_15.PNG" alt="CINE+ EMOTION" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-emotion.html"> CINE+ EMOTION</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-emotion-hd.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_396.PNG" alt="CINE+ EMOTION HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-emotion-hd.html"> CINE+ EMOTION HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/ocs-choc.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_466.PNG" alt="OCS CHOC HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/ocs-choc.html"> OCS CHOC HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-fx.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_218.PNG" alt="CINE FX" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-fx.html"> CINE FX</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-polar.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_217.PNG" alt="CINE POLAR" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-polar.html"> CINE POLAR</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/action.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_119.PNG" alt="ACTION" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/action.html"> ACTION</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                    						
                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Famille                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                              <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-famiz.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_216.PNG" alt="CINE+ FAMIZ" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-famiz.html"> CINE+ FAMIZ</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/disney-cinemagic.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_328.PNG" alt="DISNEY CINEMAGIC" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/disney-cinemagic.html"> DISNEY CINEMAGIC</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                                </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/disney-cinemagic-hd.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_340.PNG" alt="DISNEY MAGIC HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/disney-cinemagic-hd.html"> DISNEY MAGIC HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                                </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/disney-cinemagic-1.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_329.PNG" alt="DISNEY MAGIC+1" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/disney-cinemagic-1.html"> DISNEY MAGIC+1</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                            <img src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/images/exclu.png" alt="exclu"/>
                                                                </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                    						
                                                                                                                                                                                                                                             
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Culte/Classiques                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                              <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-classic.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_16.PNG" alt="CINE+ CLASSIC" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-classic.html"> CINE+ CLASSIC</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/cine-club.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_215.PNG" alt="CINE+ CLUB" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/cine-club.html"> CINE+ CLUB</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/paramount.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_487.PNG" alt="PARAMOUNT HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/paramount.html"> PARAMOUNT HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/ocs-geants.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_370.PNG" alt="O.CINE GEANTS" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/ocs-geants.html"> O.CINE GEANTS</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/tcm.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_48.PNG" alt="TCM" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/tcm.html"> TCM</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                                                                            <span class="channel-packs">
	                            <a href="http://www.lesoffrescanal.fr/tcm-hd.html">
                                    <div class="image">
                                        <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_375.PNG" alt="TCM HD" />
                                    </div>
                                </a>
                                <div class="name-channel">
                                    <span><a href="http://www.lesoffrescanal.fr/tcm-hd.html"> TCM HD</a></span>
                                </div>
				    
                                <div class="exclu-channel">
              			                                        </div>
                                <div class="modality-channel">
                                    <span></span>
                                </div>
                            </span>
                                                    						
                                                                                                                                                                                                                                                                                                                                                     
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Chaînes TNT                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tf1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_91.PNG" alt="TF1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tf1.html"> TF1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-2.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_92.PNG" alt="FRANCE 2" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-2.html"> FRANCE 2</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-3.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_93.PNG" alt="FRANCE 3" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-3.html"> FRANCE 3</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-5.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_95.PNG" alt="FRANCE 5" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-5.html"> FRANCE 5</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_96.PNG" alt="M6" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6.html"> M6</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/arte.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_94.PNG" alt="ARTE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/arte.html"> ARTE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/direct8.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_231.PNG" alt="D8" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/direct8.html"> D8</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/w9.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_233.PNG" alt="W9" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/w9.html"> W9</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tmc.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_5.PNG" alt="TMC" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tmc.html"> TMC</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nt1.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_232.PNG" alt="NT1" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nt1.html"> NT1</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj12.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_245.PNG" alt="NRJ 12" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj12.html"> NRJ 12</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/lcp.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_89.PNG" alt="LCP" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/lcp.html"> LCP</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-4.html">
                                <div class="image">
                                    <img src="http://newmedia.canal-plus.com/image/01/6/25016.png" alt="FRANCE 4" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-4.html"> FRANCE 4</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bfm-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_247.PNG" alt="BFM TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bfm-tv.html"> BFM TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/i-tele.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_103.PNG" alt="I&gt;TELE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/i-tele.html"> I>TELE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/direct-star.html">
                                <div class="image">
                                    <img src="https://secure-newmedia.canal-plus.com/image/54/8/98548.png" alt="D17" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/direct-star.html"> D17</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/gulli.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_248.PNG" alt="GULLI" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/gulli.html"> GULLI</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-o.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_82.PNG" alt="FRANCE Ô" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-o.html"> FRANCE Ô</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/l-equipe-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_84.PNG" alt="L'EQUIPE TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/l-equipe-tv.html"> L'EQUIPE TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/numero-23.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_443.PNG" alt="NUMERO 23" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/numero-23.html"> NUMERO 23</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/rmc-decouverte.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_438.PNG" alt="RMC DECOUVERTE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/rmc-decouverte.html"> RMC DECOUVERTE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cherie-25.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_440.PNG" alt="CHERIE HD" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cherie-25.html"> CHERIE HD</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                                                                            <dl class="row-category-channel" style="margin-right:20px;">
                        <dt class="handle">
                            <a href="#here">
                                <span class="toggle-picto-pack"></span>
                                <span class="toggle-text-pack">
                                    Autres Chaînes                                       
                                </span>
                            </a>
                        </dt>
                     <dd class="definition">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      						
                                                                                <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bbc-world-news.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_99.PNG" alt="BBC WORLD NEWS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bbc-world-news.html"> BBC WORLD NEWS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/best-of-shopping.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_98.PNG" alt="BEST OF SHOPPING" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/best-of-shopping.html"> BEST OF SHOPPING</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bfm-business.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_286.PNG" alt="BFM BUSINESS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bfm-business.html"> BFM BUSINESS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/bloomberg-television.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_47.PNG" alt="BLOOMBERG TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/bloomberg-television.html"> BLOOMBERG TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/cnn.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_30.PNG" alt="CNN INT." />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/cnn.html"> CNN INT.</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-life.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_281.PNG" alt="EQUIDIA LIFE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-life.html"> EQUIDIA LIFE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/equidia-live.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_44.PNG" alt="EQUIDIA" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/equidia-live.html"> EQUIDIA</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/euronews.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_52.PNG" alt="EURONEWS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/euronews.html"> EURONEWS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/france-24.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_310.PNG" alt="FRANCE 24 " />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/france-24.html"> FRANCE 24 </a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/kto.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_102.PNG" alt="KTO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/kto.html"> KTO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span>Disponible uniquement par ADSL</span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/m6-boutique.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_106.PNG" alt="M6 BOUTIQUE &amp; CO" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/m6-boutique.html"> M6 BOUTIQUE & CO</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/montagne-tv.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_263.PNG" alt="MONTAGNE TV" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/montagne-tv.html"> MONTAGNE TV</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj-hits.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_339.PNG" alt="NRJ HITS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj-hits.html"> NRJ HITS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/nrj-paris.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_134.PNG" alt="NRJ PARIS" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/nrj-paris.html"> NRJ PARIS</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv5-monde.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_56.PNG" alt="TV5 MONDE" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv5-monde.html"> TV5 MONDE</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                                                                            <span class="channel-packs">
        				    <a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html">
                                <div class="image">
                                    <img src="http://epg.canal-plus.com/mycanal/img/CHN43FB/PNG/60X45/CHN43FB_262.PNG" alt="TV8 MONT-BLANC" />
                                </div>
                		    </a>
                            <div class="name-channel">
                                <span><a href="http://www.lesoffrescanal.fr/tv8-mont-blanc.html"> TV8 MONT-BLANC</a></span>
                            </div>
                            <div class="exclu-channel">
                                                        </div>
                            <div class="modality-channel">
                                <span></span>
                            </div>
                            </span>
                                                         
                        <div class="clearer"></div>
                    </dd>
                </dl>
                    
                    
                        </div>
		                
		<script>
			if($$('#packChannelOptionTabList li.selected').length==0){
				$$('#packChannelOptionTabList li')[0].addClassName('selected');
				var className = $$('#packChannelOptionTabList li')[0].readAttribute('data-channel');
				$$('#packChannelOptions .packChannelOptionContent.' + className)[0].addClassName('active');
			}
		</script>

    </div>
</div>

<script type="text/javascript">
    +function($) {
    if($('#packChannelOptions').length > 0) {
        $('#packChannelOptionTabList li').click(function() {
            // If current tab is not selected yet
            if(!$(this).hasClass('selected')) {
                // Removes previously selected tab, then select current one
                $('#packChannelOptionTabList li.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            var className = $(this).attr('data-channel');
            $('#packChannelOptions .packChannelOptionContent').removeClass('active');
            $('#packChannelOptions .packChannelOptionContent.' + className).addClass('active');
            $('img.lazy').lazyload();
	    // Sets tab change on channel options
        });
    }
    }(jQuery);
	
</script>
        </div>
        
        <div class="footer-before-container"><section class="container" id="subscriptionReasons">
<div>
<p class="sectionTitle">5 bonnes raisons de s'abonner</p>
<ul>
<li class="blue immediate">
<p>Abonnez-vous<br /><strong>en quelques clics</strong></p>
</li>
<li class="sevenDays">
<p>Paiement <strong>s&eacute;curis&eacute;</strong></p>
</li>
<li class="offers">
<p>Aide en ligne<br /><br /><strong>Des conseillers &agrave; votre disposition <br />de 8h &agrave; 21h</strong></p>
</li>
<li class="blue freeShipping">
<p><strong>Vos programmes</strong> imm&eacute;diatement</p>
</li>
<li class="securePayment">
<p><strong><span class="big">14 jours</span></strong> pour changer d'avis</p>
</li>
</ul>
</div>
</section><section class="container" id="otherSubscriptionWays">
<div>
<div class="mainSubscription">
<p class="sectionTitle">Les autres fa&ccedil;ons de s'abonner</p>
<ul>
<li class="phone">
<p class="title">Par t&eacute;l&eacute;phone</p>
<p>Appelez le 3910 (0,23&euro;/min TTC depuis un poste fixe).</p>
</li>
<li class="television">
<p class="title">Sur votre t&eacute;l&eacute;vision</p>
<p>Vous &ecirc;tes abonn&eacute; ADSL ? Rendez-vous sur la cha&icirc;ne 4 et appuyez sur la touche OK.</p>
</li>
<li class="mobile">
<p class="title">Sur votre mobile</p>
<p>Rendez-vous sur www.lesoffrescanal.fr depuis votre smartphone.</p>
</li>
</ul>
</div>
<div class="shopSubscription">
<p class="title">En boutique</p>
<p>la plus proche de chez vous.</p>
<a class="button" style="color: #249ddd; border-color: #249ddd; outline-color: #249ddd;" href="http://www.lesoffrescanal.fr/distributeur"> Trouver un distributeur </a></div>
</div>
</section></div>        
<div id="inqC2C3ImgContainer" style="position: fixed; top: 120px; margin-left: -40px;">&nbsp;</div>
<footer class="container" id="footerLinkMap">
<p class="border">&nbsp;</p>
<div>
<ul class="mapList">
<li class="offers" style="height: 230px;">
<p class="title">Toutes nos offres</p>
<ul>
<li><a title="CANAL+" href="http://www.lesoffrescanal.fr/offre-tv/canal-plus">CANAL+</a></li>
<li><a title="Canal+ la cha&icirc;ne" href="http://www.lesoffrescanal.fr/offre-tv/canal-plus/regarder-canal">CANAL+ la cha&icirc;ne</a></li>
<li><a title="Les cha&icirc;nes CANAL+" href="http://www.lesoffrescanal.fr/offre-tv/canal-plus/chaines-tv">Les cha&icirc;nes CANAL+</a></li>
<li><a href="http://www.lesoffrescanal.fr/carte-prepayee">CANAL+ Pr&eacute;pay&eacute;</a></li>
<li><a title="CANALSAT" href="http://www.lesoffrescanal.fr/offre-tv/chaine-canalsat">CANALSAT</a></li>
<li><a title="CANALSAT Pack Panorama" href="http://www.lesoffrescanal.fr/offre-tv/chaine-canalsat/canalsat-panorama">CANALSAT Panorama</a></li>
<li><a href="http://www.lesoffrescanal.fr/carte-prepayee-canalsat">CANALSAT Pr&eacute;pay&eacute;</a></li>
<li><a title="Parrainage" href="http://www.lesoffrescanal.fr/offre-parrainage">Parrainage</a></li>
</ul>
</li>
<li class="innovativeServices">
<p class="title">Services inclus</p>
<ul>
<li><a title="MyCanal" href="http://www.lesoffrescanal.fr/service-canal-plus/mycanal">MyCanal</a></li>
<li><a title="Voir &agrave; la demande" href="http://www.lesoffrescanal.fr/service-canal-plus/tv-a-la-demande">Voir &agrave; la demande</a></li>
<li><a title="Enregistrement et contr&ocirc;le du direct" href="http://www.lesoffrescanal.fr/service-canal-plus/enregistrer-tv">Enregistrement et contr&ocirc;le du direct</a></li>
<li><a title="Multi-&eacute;crans" href="http://www.lesoffrescanal.fr/service-canal-plus/option-multi-ecran">Multi-&eacute;crans</a></li>
<li><a title="Second &eacute;cran TV" href="http://www.lesoffrescanal.fr/service-canal-plus/second-ecran">Second &eacute;cran TV</a></li>
<li><a title="Eureka" href="http://www.lesoffrescanal.fr/service-canal-plus/eureka-idee-film">Eur&eacute;ka</a></li>
<li><a title="Campus" href="http://www.lesoffrescanal.fr/service-canal-plus/campus-tv-educative">Campus</a></li>
</ul>
</li>
<li class="receptionModes">
<p class="title">Modes de r&eacute;ception</p>
<ul>
<li><a title="ADSL/Fibre" href="http://www.lesoffrescanal.fr/modes-de-reception/">ADSL/fibre</a></li>
<li><a title="Satellite" href="http://www.lesoffrescanal.fr/modes-de-reception/satellite">Satellite</a></li>
<li><a title="C&acirc;ble" href="http://www.lesoffrescanal.fr/modes-de-reception">C&acirc;ble</a></li>
<li><a title="TNT" href="http://www.lesoffrescanal.fr/modes-de-reception/tnt/">TNT</a></li>
</ul>
</li>
<li class="assistance">
<p class="title">Assistance</p>
<ul>
<li><a title="Plan du site" href="http://www.lesoffrescanal.fr/catalog/seo_sitemap/category">Plan du site</a></li>
<li><a title="Mentions l&eacute;gales" href="http://www.lesoffrescanal.fr/decouvrir-et-s-abonner/assistance/mentions-legales">Mentions l&eacute;gales</a></li>
<li><a title="CGA" href="http://static.lesoffrescanal.fr/cga_cgv/CGA_C+CSAT.pdf" target="_blank">CGA</a></li>
<li><a title="CGA" href="http://static.lesoffrescanal.fr/cga_cgv/FORM_RETRACTATION.pdf" target="_blank">R&eacute;tractation</a></li>
<li><a href="http://www.lesoffrescanal.fr/privacy-policy-cookie-restriction-mode">Cookies</a></li>
<li class="sourdline"><a title="" href="http://www.lesoffrescanal.fr/sourds-et-malentendants">Sourdline</a></li>
</ul>
</li>
<li class="socialLinks"><a class="twitter" title="" href="https://twitter.com/canalplus" target="_blank">Twitter</a><hr /><a class="facebook" title="" href="https://fr-fr.facebook.com/canalplus" target="_blank">Facebook</a></li>
</ul>
</div>
</footer><footer class="container" id="footerLinks">
<div>
<ul>
<li><a title="CANAL+.FR" href="http://www.canalplus.fr/#sc_intcmp=LOC:FOOTER:LIENPERMANENT:CANALPLUS" target="_blank">canalplus.fr</a></li>
<li><a title="CANALSAT.FR" href="http://www.canalsat.fr/#sc_intcmp=LOC:FOOTER:LIENPERMANENT:CANALSAT" target="_blank">canalsat.fr</a></li>
<li><a title="CANALPLAY" href="http://www.canalplay.com#sc_intcmp=LOC:FOOTER:LIENPERMANENT:CANALPLAY" target="_blank">Canalplay</a></li>
<li><a title="ESPACE CLIENT" href="http://espaceclient.canalplus.canal-plus.com/#sc_intcmp=LOC:FOOTER:LIENPERMANENT:EC" target="_blank">Espace client</a></li>
<li><a title="STUDIOCANAL" href="http://www.studiocanal.com/#sc_intcmp=LOC:FOOTER:LIENPERMANENT:STUDIOCANAL" target="_blank">Studiocanal</a></li>
<li><a title="CANAL ESPACE PRO" href="http://canalespacepro.canal-plus.com#sc_intcmp=LOC:FOOTER:LIENPERMANENT:ESPACEPRO" target="_blank">Canal espace pro</a></li>
<li>© CANAL+ 2014</li>
</ul>
</div>
</footer>
<script language="JavaScript" src="http://www.lesoffrescanal.fr/skin/frontend/enterprise/loc/js/script.js"></script>                        

        <!-- SiteCatalyst code version: H.11. -->
        <script language="JavaScript" src="http://www.lesoffrescanal.fr/js/omniture/s_code_mplus.php?account=cplusglobalprod,cplusoffrescanalprod&filters=javascript:,canal-plus.com,canal-bis.com,lesoffrescanal.fr,mplus,127.0.0.1,localhost"></script>
        <script language="JavaScript">

        
    </script>
    <script language="JavaScript">if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')</script>
    <!-- End SiteCatalyst code version: H.11. -->

    <!-- Call to Action -->
    <script type="text/javascript">
        function callToAction(events, seVar, eVar60) {
        	s.events=events;
        	eval(seVar+'=eVar60'); // eVar60 ne doit pas contenir des operations mathematiques.
            var eVar = seVar.split(".");
            s.linkTrackVars=eVar[1]+',events';
            s.linkTrackEvents=events;
            s.tl(this, 'o', eVar60);
        }

        function callToActionWithMathsOperator(events, seVar, eVar) {
        	switch (seVar)
        	{
                case "s.eVar27":
                    s.events=events;
                    s.eVar27=eVar;
                    s.linkTrackVars='eVar27,events';
                    s.linkTrackEvents=events;
                    s.tl(this, 'o', eVar);
                break;
                case "s.eVar28":
                    s.events=events;
                    s.eVar28=eVar;
                    s.linkTrackVars='eVar28,events';
                    s.linkTrackEvents=events;
                    s.tl(this, 'o', eVar);
                break;
            	case "s.eVar35":
                    s.events=events;
                    s.eVar35=eVar;
                    s.linkTrackVars='eVar35,events';
                    s.linkTrackEvents=events;
                    s.tl(this, 'o', eVar);
            	break;
                case "s.eVar41":
                    s.eVar41=eVar;
                    s.linkTrackVars='eVar41';
                    s.tl(this, 'o', eVar);
                break;
                case "s.eVar42":
                    s.events=events;
                    s.eVar42=eVar;
                    s.linkTrackVars='eVar42,events';
                    s.linkTrackEvents=events;
                    s.tl(this, 'o', eVar);
                break;
                case "s.eVar54":
                	s.eVar55=events;
                    s.eVar54=eVar;
                    s.linkTrackVars='eVar54,eVar55';
                    s.tl(this, 'o', eVar);
                break;
                case "s.eVar54_tntsat":
                    s.eVar54=eVar;
                    s.linkTrackVars='eVar54';
                    s.tl(this, 'o', eVar);
                break;
                case "Remove":
                    s.events=events;
                    s.linkTrackVars="events";
                    s.linkTrackEvents=events;
                    s.tl(this,'o','LOC - '+''+' - Vider');
                break;
                case "Produit":
                	s.pageName="LOC -  - Offre - " + eVar
                	s.prop4="LOC - "
            		s.prop5="LOC -  - Offre"
            		s.prop6="LOC - Tous Tunnel - Offre - " + eVar
            		s.prop7="LOC -  - Offre - " + eVar
                    s.events=events;
                    //s.linkTrackVars="events,pageName,prop4,prop5,prop6,prop7";
                    copyPropInVar(s);
                    s.linkTrackEvents=events;
                    s.tl(this, 'o', eVar);
                break;
                case "rebond":
                	s.eVar41="LOC - Rebond - EspaceClient";
                	s.eVar52="Abonne";
                    s.linkTrackVars="eVar52,eVar41";
                    s.tl(this, 'o', eVar);
                break;
                case "presentation_antenne":
                    s.pageName="LOC -  - Materiel - Presentation antenne"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Materiel"
                    s.prop6="LOC - Tous Tunnel - Materiel - Presentation antenne"
                    s.prop7="LOC -  - Materiel - Presentation antenne"
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7";
                	copyPropInVar(s);
                    s.tl(this, 'o', 'presentation_antenne');
                break;
                case "presentation_materiel":
                    s.pageName="LOC -  - Materiel - Presentation materiel"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Materiel"
                    s.prop6="LOC - Tous Tunnel - Materiel - Presentation materiel"
                    s.prop7="LOC -  - Materiel - Presentation materiel"
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7";
                	copyPropInVar(s);
                    s.tl(this, 'o', 'presentation_materiel');
                break;
                case "isp-success":
                    s.pageName="LOC -  - Materiel - Eligibilite OK"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Materiel"
                    s.prop6="LOC - Tous Tunnel - Materiel - Eligibilite OK"
                    s.prop7="LOC -  - Materiel - Eligibilite OK"
                    s.events=""
                    s.linkTrackVars="events,pageName,prop4,prop5,prop6,prop7";
                    s.linkTrackEvents=s.events;
                    s.t();
                break;
                case "eligibility-failure":
                    s.pageName="LOC -  - Materiel - Eligibilite KO"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Materiel"
                    s.prop6="LOC - Tous Tunnel - Materiel - Eligibilite KO"
                    s.prop7="LOC -  - Materiel - Eligibilite KO"
                   	s.events=""
                    s.linkTrackVars="pageName,prop4,prop5,prop6,prop7";
                    s.linkTrackEvents=s.events;
                    s.t();
                break;
                case "ETAPE5":
                                            if($('cart_print')) {
                            var hrefprint = $('cart_print').getAttribute("href");
                            if (hrefprint.indexOf("ETAPE7") != '-1') {hrefprint = hrefprint.gsub("ETAPE7", "ETAPE5");var etape = "ETAPE7";}
                            else if (hrefprint.indexOf("ETAPE6") != '-1') {hrefprint = hrefprint.gsub("ETAPE6", "ETAPE5");var etape = "ETAPE6";}
                            $('cart_print').setAttribute("href", hrefprint);
                        }
                        s.pageURL='http://www.lesoffrescanal.fr/checkout/onepage/index/cgv/1/#sc_intcmp=LOC:'+':'+etape+':RETOUR'
                        s.pageName="LOC -  - Coordonnees - Coordonnees Personnelles"
                        s.prop4="LOC - "
                        s.prop5="LOC -  - Coordonnees"
                        s.prop6="LOC - Tous Tunnel - Coordonnees - Coordonnees Personnelles"
                        s.prop7="LOC -  - Coordonnees - Coordonnees Personnelles"
                        s.linkTrackVars="pageURL,pageName,prop4,prop5,prop6,prop7";
                        s.t();
                                    break;
                case "ETAPE5-LIVRAISON":
                    if(events == "RETOUR") {
                        if($('cart_print')) {
                            var hrefprint = $('cart_print').getAttribute("href");
                            if (hrefprint.indexOf("ETAPE7") != '-1') hrefprint = hrefprint.gsub("ETAPE7", "ETAPE6");
                            $('cart_print').setAttribute("href", hrefprint);
                        }
                        s.pageURL='http://www.lesoffrescanal.fr/checkout/onepage/index/cgv/1/#sc_intcmp=LOC::ETAPE7:RETOUR';
                    }
                    else
                        s.pageURL='http://www.lesoffrescanal.fr/checkout/onepage/index/cgv/1/#sc_intcmp=LOC::ETAPE5:CONTINUER';
                    s.pageName="LOC -  - Livraison"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Livraison"
                    s.prop6="LOC - Tous Tunnel - Livraison"
                	s.prop7=""
                    s.linkTrackVars="pageURL,pageName,prop4,prop5,prop6,prop7";
                    s.t();
                break;
                case "ETAPE5-LISTE":
                    s.pageName="LOC -  - Coordonnees - Liste point relai"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Coordonnees"
                    s.prop6="LOC - Tous Tunnel - Coordonnees - Liste point relai"
                    s.prop7="LOC -  - Coordonnees - Liste point relai"
                    s.linkTrackVars="pageName,prop4,prop5,prop6,prop7";
                    s.t();
                break;
                case "ETAPE6":
                	s.pageURL='http://www.lesoffrescanal.fr/checkout/onepage/index/cgv/1/#sc_intcmp=LOC::ETAPE6:CONTINUER';
                    s.pageName="LOC -  - Paiement - Coordonnees bancaires"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Paiement"
                    s.prop6="LOC - Tous Tunnel - Paiement - Coordonnees bancaires"
                    s.prop7="LOC -  - Paiement - Coordonnees bancaires"
                    s.linkTrackVars="pageURL,pageName,prop4,prop5,prop6,prop7";
                    s.t();
                break;

                case "activation_erreur_technique":
                    s.pageName="LOC - Activation OPP - Offre - Erreur technique"
                    s.prop4="LOC - Activation OPP"
                    s.prop5="LOC - Activation OPP - Offre"
                    s.prop6="LOC - Activation OPP - Offre - Erreur technique"
                    s.prop19="LOC - OPP - Erreur technique";
                    s.linkTrackVars="pageName,prop4,prop5,prop6,prop19";
                    s.t();
                break;
                case "ACTIVATED":
                    s.pageName="LOC - Activation OPP - Offre - Carte deja activee"
                    s.prop4="LOC - Activation OPP"
                    s.prop5="LOC - Activation OPP - Offre"
                    s.prop6="LOC - Activation OPP - Offre - Carte deja activee"
                    s.prop19="LOC - OPP - Carte deja activee";
                    s.linkTrackVars="pageName,prop4,prop5,prop6,prop19";
                    s.t();
                break;
                case "INVALID":
                    s.pageName="LOC - Activation OPP - Offre - Carte invalide"
                    s.prop4="LOC - Activation OPP"
                    s.prop5="LOC - Activation OPP - Offre"
                    s.prop6="LOC - Activation OPP - Offre - Carte invalide"
                    s.prop19="LOC - OPP - Carte invalide";
                    s.linkTrackVars="pageName,prop4,prop5,prop6,prop19";
                    s.t();
                break;
                case "INVALID_CARD_NUMBER_or_BLOCKED":
                    s.pageName="LOC - Activation OPP - Offre - Numero invalide"
                    s.prop4="LOC - Activation OPP"
                    s.prop5="LOC - Activation OPP - Offre"
                    s.prop6="LOC - Activation OPP - Offre - Numero invalide"
                    s.prop19="LOC - OPP - Numero invalide";
                    s.linkTrackVars="pageName,prop4,prop5,prop6,prop19";
                    s.t();
                break;
                case "ma_carte_prepayee":
                    s.pageName="LOC - Activation OPP - Offre - Ma carte prepayee"
                    s.prop4="LOC - Activation OPP"
                    s.prop5="LOC - Activation OPP - Offre"
                    s.prop6="LOC - Activation OPP - Offre - Ma carte prepayee"
                    s.linkTrackVars="pageName,prop4,prop5,prop6";
                    s.t();
                break;
                case "LOC-Activation-OPP-FAI-Eligibilite":
                                            s.pageName="LOC - Activation OPP - FAI - Eligibilite"
                        s.prop4="LOC - Activation OPP"
                        s.prop5="LOC - Activation OPP - FAI"
                        s.prop6="LOC - Activation OPP - FAI - Eligibilite"
                        //s.linkTrackVars="pageName,prop4,prop5,prop6";
                    	copyPropInVar(s);
                        s.tl(this, 'o', 'Eligibilite');
                                    break;
                case "LOC-Activation-OPP-FAI-EligibiliteKO":
                                            s.pageName="LOC - Activation OPP - FAI - Eligibilite - Eligibilite KO"
                        s.prop4="LOC - Activation OPP"
                        s.prop5="LOC - Activation OPP - FAI"
                        s.prop6="LOC - Activation OPP - FAI - Eligibilite"
                        s.prop7="LOC - Activation OPP - FAI - Eligibilite - Eligibilite KO"
                	    s.prop17="ADSL"
                        s.prop19="LOC - OPP - Non eligible OPP";
                        s.linkTrackVars="pageName,prop4,prop5,prop6,prop7,prop17,prop19";
                        s.t();
                                    break;
                case "LOC-Activation-OPP-FAI-EligibiliteOK":
                                            s.pageName="LOC - Activation OPP - FAI - Eligibilite - Eligibilite OK"
                        s.prop4="LOC - Activation OPP"
                        s.prop5="LOC - Activation OPP - FAI"
                        s.prop6="LOC - Activation OPP - FAI - Eligibilite"
                        s.prop7="LOC - Activation OPP - FAI - Eligibilite - Eligibilite OK"
                    	s.prop17="ADSL"
                        s.linkTrackVars="pageName,prop4,prop5,prop6,prop7,prop17";
                        s.t();
                                    break;
                case "renewal_address_ko":
                    s.pageName="TNTS - Renouvellement - Coordonnees - Erreur saisie"
                    s.prop4="TNTS - Renouvellement"
                    s.prop5="TNTS - Renouvellement - Coordonnees"
                    s.prop6="TNTS - Renouvellement - Coordonnees - Erreur saisie"
                    s.prop19="TNTS - Coordonnees non valides"
                    s.events="event3"
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop19";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Erreur saisie");
                break;
                case "renewal_address_livraison_ko":
                    s.pageName="TNTS - Renouvellement - Livraison - Erreur saisie"
                    s.prop4="TNTS - Renouvellement"
                    s.prop5="TNTS - Renouvellement - Livraison"
                    s.prop6="TNTS - Renouvellement - Livraison - Erreur saisie"
                    s.prop19="TNTS - Livraison non valide"
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop19";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Erreur saisie");
                break;
                case "renewal_address_ok":
                    s.channel="lesoffrescanal.fr"
                    s.pageName="TNTS - Renouvellement - Livraison"
                    s.prop4="TNTS - Renouvellement"
                    s.prop5="TNTS - Renouvellement - Livraison"
                    s.eVar4=s.prop4;
                    s.eVar5=s.prop5;
                    s.events="event3"
                    s.linkTrackVars="pageName,prop4,prop5,eVar4,eVar5,events,channel";
                    s.linkTrackEvents=s.events;
                    //copyPropInVar(s);
                    //s.t();
                    s.tl(this, 'o', "Livraison Choix");
                break;
                case "discovery_offer_ko":
                    s.pageName="TNTS - Decouverte - Eligibilite non valide"
                    s.prop4="TNTS - Decouverte"
                    s.prop5="TNTS - Decouverte - Eligibilite non valide"
                    s.prop19="TNTS - Carte non valide"
                    s.events="event3"
                    //s.linkTrackVars="pageName,prop4,prop5,prop19";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Eligibilite non valide");
                break;
                case "discovery_create_address_ko":
                    s.pageName="TNTS - Decouverte - Coordonnees - Erreur saisie"
                    s.prop4="TNTS - Decouverte"
                    s.prop5="TNTS - Decouverte - Coordonnees"
                    s.prop6="TNTS - Decouverte - Coordonnees - Erreur saisie"
                    s.prop19="TNTS - Coordonnees non valides"
                    //s.linkTrackVars="pageName,prop4,prop5,prop19";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Erreur saisie");
                break;
                case "discovery_edit_address_ko":
                    s.pageName="TNTS - Decouverte - Coordonnees - Erreur saisie"
                    s.prop4="TNTS - Decouverte"
                    s.prop5="TNTS - Decouverte - Coordonnees"
                    s.prop6="TNTS - Decouverte - Coordonnees - Erreur saisie"
                    s.prop19="TNTS - Coordonnees non valides"
                    //s.linkTrackVars="pageName,prop4,prop5,prop19";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Erreur saisie");
                break;
                case "error_login":
                    s.pageName="LOC - Suivi commande - Login ko"
                    s.prop4="LOC - Suivi commande"
                    s.prop5="LOC - Suivi commande - Login ko"

                    //s.linkTrackVars="pageName,prop4,prop5";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Login ko");
                break;
                case "error_login_opp":
                                            s.pageName="LOC - Activation OPP - Coordonnees - Authentification - Mot de passe incorrect"
                        s.prop4="LOC - Activation OPP"
                        s.prop5="LOC - Activation OPP - Coordonnees"
                        s.prop6="LOC - Activation OPP - Coordonnees - Authentification - Mot de passe incorrect"
                        s.prop7="LOC - Activation OPP - Coordonnees - Authentification - Mot de passe incorrect"
                        s.prop19="LOC - OPP - Mot de passe incorrect";
                        //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7,prop19";
                        copyPropInVar(s);
                        s.tl(this, 'o', "Login KO");
                                    break;
                case "error_login_tnt":
                        s.pageName="TNTS - Decouverte - Coordonnees - Authentification KO"
                        s.prop4="TNTS - Decouverte"
                        s.prop5="TNTS - Decouverte - Coordonnees"
                        s.prop6="TNTS - Decouverte - Coordonnees - Authentification KO"
                        //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7,prop19";
                        copyPropInVar(s);
                        s.tl(this, 'o', "Login KO");
                break;
                case "renvoi_mdp_tnt":
                    s.pageName="TNTS - Decouverte - Coordonnees - Renvoi MDP"
                    s.prop4="TNTS - Decouverte"
                    s.prop5="TNTS - Decouverte - Coordonnees"
                    s.prop6="TNTS - Decouverte - Coordonnees - Renvoi MDP"
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7,prop19";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Renvoi MDP");
            	break;
                case "doublon_eligibilite":
                    s.pageName="LOC -  - Materiel - Rejet"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Materiel"
                    s.prop6="LOC - Tous Tunnel - Materiel - Rejet"
                    s.prop7="LOC -  - Materiel - Rejet"
                    s.events="";
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7";
                	copyPropInVar(s);
                    s.tl(this, 'o', 'Rejet');
                break;
                case "doublon_coordonnees":
                    s.pageName="LOC -  - Coordonnees - Rejet"
                    s.prop4="LOC - "
                    s.prop5="LOC -  - Coordonnees"
                    s.prop6="LOC - Tous Tunnel - Coordonnees - Rejet"
                    s.prop7="LOC -  - Coordonnees - Rejet"
                    s.events="";
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7";
                	copyPropInVar(s);
                    s.tl(this, 'o', 'Rejet');
                break;
                case "clean_cart":
                    s.events="scRemove";
                    s.linkTrackVars='events';
                    s.linkTrackEvents='scRemove';
                    //copyPropInVar(s);
                    //s.t();
                    s.tl(this, 'o', "LOC -  - Vider");
                break;
                case "les-packs":
                    s.pageName=eVar
                    props=eVar.split("-");
                    s.prop5=props[0] + " - " + props[1]
                    s.prop6=props[0] + " - " + props[1] + " - " + props[2]
                    s.prop7=props[0] + " - " + props[1] + " - " + props[2] + " - " + props[3]
                    s.events="";
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7";
                	copyPropInVar(s);
                    s.tl(this, 'o', 'les-packs');
                break;
                case "cart_error":
                    s.pageName="LOC - CSAT - Formules - PACK PANORAMA"
                    s.prop4="LOC - Tunnel"
                    s.prop5="LOC - Tunnel - Erreur panier"
                    s.prop6="LOC - Tous Tunnel - Erreur panier"
                    //s.linkTrackVars="pageName,prop4,prop5,prop6,prop7,prop19";
                    copyPropInVar(s);
                    s.tl(this, 'o', "Erreur panier");
            	break;

            }
        }

/*      Event click start   */

        if($('cart_truncate')) $('cart_truncate').observe('click',function(event){callToActionWithMathsOperator('scRemove','Remove');});
        if($('change_subscription')) $('change_subscription').observe('click', function(event){callToActionWithMathsOperator('','rebond');});
        if($('e_answer_non')) $('e_answer_non').observe('click', function(event){callToActionWithMathsOperator('','presentation_antenne');});
        if($('e_answer_oui')) $('e_answer_oui').observe('click', function(event){callToActionWithMathsOperator('','presentation_materiel');});
        if($('cart_print')) $('cart_print').observe('click', function(event){callToActionWithMathsOperator('event19', 's.eVar42', '______14082014-20:18:57');});
        if((document.location.href.indexOf("tntsat") == -1)) {
            if($('billing:accept_info')) $('billing:accept_info').observe('click', function(event){if($('billing:accept_info').checked == true)callToActionWithMathsOperator('event6', 's.eVar28', 'LOC - OPP - Offres commerciales');});
        }
/*      Event click end     */

/*      AJAX PAGE start     */

        
        
        document.observe('checkout:ETAPE5-LIVRAISON', function() {
        	if($('cart_print')) {
                var hrefprint = $('cart_print').getAttribute("href");
                if (hrefprint.indexOf("ETAPE5-LIVRAISON") == '-1' && hrefprint.indexOf("ETAPE5-LISTE") == '-1') hrefprint = hrefprint.gsub("ETAPE5", "ETAPE6");
                $('cart_print').setAttribute("href", hrefprint);
        	}
        	if($('cart_print'))
        		callToActionWithMathsOperator('', 'ETAPE5-LIVRAISON', '');
        });

        document.observe('checkout:payment', function() {
            if($('cart_print')) {
                var hrefprint = $('cart_print').getAttribute("href");
                if (hrefprint.indexOf("ETAPE6") == '-1' && hrefprint.indexOf("ETAPE5-LISTE") == '-1') hrefprint = hrefprint.gsub("ETAPE5", "ETAPE7");
                else if (hrefprint.indexOf("ETAPE6") != '-1') hrefprint = hrefprint.gsub("ETAPE6", "ETAPE7");
                $('cart_print').setAttribute("href", hrefprint);
                callToActionWithMathsOperator('', 'ETAPE6');
            }
        });


        document.observe('qas:error', function() {
                if((document.location.href.indexOf("tntsat/discovery/create") != -1)) {
                    callToActionWithMathsOperator('', 'discovery_create_address_ko');
                }
                if((document.location.href.indexOf("tntsat/discovery/edit") != -1)) {
                    callToActionWithMathsOperator('', 'discovery_edit_address_ko');
                }
                if((document.location.href.indexOf("tntsat/renewal/address") != -1)) {
                    if ( document.getElementById('checkout-step-shipping_method').style.display=='none') {
                        callToActionWithMathsOperator('', 'renewal_address_ko');
                    } else {
                    	callToActionWithMathsOperator('', 'renewal_address_livraison_ko');
                    }
                }
        });

        if((document.location.href.indexOf("tntsat/renewal/address") != -1)) {
            document.observe('qas:ok', function() {
                callToActionWithMathsOperator('', 'renewal_address_ok');
            });
        }

        if((document.location.href.indexOf("tntsat/discovery/forgotpassword") != -1)) {
            callToActionWithMathsOperator('', 'renvoi_mdp_tnt');
        }
        
        if((document.location.href.indexOf("cart-error") != -1)) {
            callToActionWithMathsOperator('', 'cart_error');
            callToActionWithMathsOperator('', 'clean_cart', '');
        }
        
        document.observe('doublon:eligibilite', function() {
            callToActionWithMathsOperator('', 'doublon_eligibilite', '');
         });
        document.observe('doublon:coordonnees', function() {
            callToActionWithMathsOperator('', 'doublon_coordonnees', '');
         });
        document.observe('doublon:clean', function() {
            callToActionWithMathsOperator('', 'clean_cart', '');
         });
/*      AJAX PAGE end     */

    </script>
    <!-- End Call to Action -->
    <script type="text/javascript" src="http://eulerian.canal-plus.com/ea.js"></script>
        <script type="text/javascript">
        function callEulerianInAjaxPage(Var,Var2) {
            switch (Var)
            {
                case "eligibility-failure":
                    /*<![CDATA[*/
                    EA_collector([
                        "path", "LOC -  - Materiel - Eligibilite KO"
                    ]); /*]]>*/
                break;
                case "isp-success":
                    /*<![CDATA[*/
                    EA_collector([
                        "path", "LOC -  - Materiel - Eligibilite OK"
                    ]); /*]]>*/
                break;
                case "LOC-Activation-OPP-FAI-EligibiliteKO":
                                            /*<![CDATA[*/
                        EA_collector([
                            "path", "LOC - Activation OPP - Materiel - Eligibilite KO"
                        ]); /*]]>*/
                                    break;
                case "LOC-Activation-OPP-FAI-EligibiliteOK":
                	                        /*<![CDATA[*/
                        EA_collector([
                            "path", "LOC - Activation OPP - Materiel - Eligibilite OK"
                        ]); /*]]>*/
                                    break;
                case "produit":
                	                break;
                 case "clean_cart":
                    /*<![CDATA[*/
                    EA_collector([
                        "email", ""
                        ,"scart",    "1"
                                            ]); /*]]>*/
                break;
                case "doublon_coordonnees":
                    /*<![CDATA[*/
                    EA_collector([
                        "path", "LOC -  - Coordonnees - Rejet"
                    ]); /*]]>*/
                break;
                case "doublon_eligibilite":
                    /*<![CDATA[*/
                    EA_collector([
                        "path", "LOC -  - Materiel - Rejet"
                    ]); /*]]>*/
                break;
                case "packs":
                    /*<![CDATA[*/
                    EA_collector([
                        "path", Var2
                    ]); /*]]>*/
                break;
            }
        }
        document.observe('doublon:clean', function() {
            callEulerianInAjaxPage('clean_cart', '');
         });
         document.observe('doublon:coordonnees', function() {
            callEulerianInAjaxPage('doublon_coordonnees', '');
         });
         document.observe('doublon:eligibilite', function() {
            callEulerianInAjaxPage('doublon_eligibilite', '');
         });
    </script>
        <script type="text/javascript">
        function callHavasInAjaxPage(Var, produit) {
            switch (Var)
            {
                case "produit":
                	var axel = Math.random() + "";
                    var a = axel * 10000000000000;
                    $('iframe-havas').src="http://fls.doubleclick.net/activityi;src=3023992;type=canalp01;cat=ptnp01;u14=;u20=jmje0j9e972k6bj9moetgqf9m5;u11="+produit+";ord=" + a + "?";
                break;
                case "packs":
                	var axel = Math.random() + "";
                    var a = axel * 10000000000000;
                    $('iframe-havas').src="http://fls.doubleclick.net/activityi;src=3023992;type=canalp01;cat=edito01;u20=jmje0j9e972k6bj9moetgqf9m5;u11="+produit+";ord=" + a + "?";
                break;
            }
        }
    </script>

        <script language="javascript" type="text/javascript" charset="utf-8" src="https://canal.inq.com/chatskins/launch/inqChatLaunch3000380.js"></script>
    <script type="text/javascript">_satellite.pageBottom();</script>
    </body>
</html>
